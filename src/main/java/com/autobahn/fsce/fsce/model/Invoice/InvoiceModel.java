package com.autobahn.fsce.fsce.model.Invoice;

import java.util.List;

public class InvoiceModel {

    private String status;
    private List<InvoiceSuccess> successes;
    private  List<FailedInvoice> failed;


    public InvoiceModel() {
    }

    public InvoiceModel(String status, List<InvoiceSuccess> successes, List<FailedInvoice> failed) {
        this.status = status;
        this.successes = successes;
        this.failed = failed;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<InvoiceSuccess> getSuccesses() {
        return successes;
    }

    public void setSuccesses(List<InvoiceSuccess> successes) {
        this.successes = successes;
    }

    public List<FailedInvoice> getFailed() {
        return failed;
    }

    public void setFailed(List<FailedInvoice> failed) {
        this.failed = failed;
    }
}
