package com.autobahn.fsce.fsce.model.Invoice;


public class InvoiceSuccess {

    private String DocumentNumber;
    private String DocumentType;
    private String TransactionDate;
    private String DeliveryDate;
    private String DueDate;
    private String OutstandingAmount;
    private String TotalAmount;
    private String Currency;
    private String DealerName;
    private String DealerCode;
    private String PrincipalCode;

    public InvoiceSuccess() {
    }


     public InvoiceSuccess(String documentNumber, String documentType, String transactionDate, String deliveryDate, String dueDate, String outstandingAmount, String totalAmount, String currency, String dealerName, String dealerCode, String principalCode) {
        DocumentNumber = documentNumber;
        DocumentType = documentType;
        TransactionDate = transactionDate;
        DeliveryDate = deliveryDate;
        DueDate = dueDate;
        OutstandingAmount = outstandingAmount;
        TotalAmount = totalAmount;
        Currency = currency;
        DealerName = dealerName;
        DealerCode = dealerCode;
        PrincipalCode = principalCode;
    }

    public String getDocumentNumber() {
        return DocumentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        DocumentNumber = documentNumber;
    }

    public String getDocumentType() {
        return DocumentType;
    }

    public void setDocumentType(String documentType) {
        DocumentType = documentType;
    }

    public String getTransactionDate() {
        return TransactionDate;
    }

    public void setTransactionDate(String transactionDate) {
        TransactionDate = transactionDate;
    }

    public String getDeliveryDate() {
        return DeliveryDate;
    }

    public void setDeliveryDate(String deliveryDate) {
        DeliveryDate = deliveryDate;
    }

    public String getDueDate() {
        return DueDate;
    }

    public void setDueDate(String dueDate) {
        DueDate = dueDate;
    }

    public String getOutstandingAmount() {
        return OutstandingAmount;
    }

    public void setOutstandingAmount(String outstandingAmount) {
        OutstandingAmount = outstandingAmount;
    }

    public String getTotalAmount() {
        return TotalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        TotalAmount = totalAmount;
    }

    public String getCurrency() {
        return Currency;
    }

    public void setCurrency(String currency) {
        Currency = currency;
    }

    public String getDealerName() {
        return DealerName;
    }

    public void setDealerName(String dealerName) {
        DealerName = dealerName;
    }

    public String getDealerCode() {
        return DealerCode;
    }

    public void setDealerCode(String dealerCode) {
        DealerCode = dealerCode;
    }

    public String getPrincipalCode() {
        return PrincipalCode;
    }

    public void setPrincipalCode(String principalCode) {
        PrincipalCode = principalCode;
    }
}
