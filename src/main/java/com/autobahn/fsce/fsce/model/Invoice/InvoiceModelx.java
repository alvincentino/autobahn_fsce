package com.autobahn.fsce.fsce.model.Invoice;

public class InvoiceModelx {

    private String currency;

    private String dealer_code;

    private String dealer_name;

    private String delivery_date;

    private String document_number;

    private String document_type;

    private String due_date;

    private String outstanding_amount;

    private String principal_code;

    private String total_amount;

    private String transaction_date;

    private String created_by;

    private String created_date;

    private String modified_by;

    private String modified_date;

    public InvoiceModelx() {
    }

    public InvoiceModelx(String currency, String dealer_code, String dealer_name, String delivery_date, String document_number, String document_type, String due_date, String outstanding_amount, String principal_code, String total_amount, String transaction_date, String created_by, String created_date, String modified_by, String modified_date) {
        this.currency = currency;
        this.dealer_code = dealer_code;
        this.dealer_name = dealer_name;
        this.delivery_date = delivery_date;
        this.document_number = document_number;
        this.document_type = document_type;
        this.due_date = due_date;
        this.outstanding_amount = outstanding_amount;
        this.principal_code = principal_code;
        this.total_amount = total_amount;
        this.transaction_date = transaction_date;
        this.created_by = created_by;
        this.created_date = created_date;
        this.modified_by = modified_by;
        this.modified_date = modified_date;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getDealer_code() {
        return dealer_code;
    }

    public void setDealer_code(String dealer_code) {
        this.dealer_code = dealer_code;
    }

    public String getDealer_name() {
        return dealer_name;
    }

    public void setDealer_name(String dealer_name) {
        this.dealer_name = dealer_name;
    }

    public String getDelivery_date() {
        return delivery_date;
    }

    public void setDelivery_date(String delivery_date) {
        this.delivery_date = delivery_date;
    }

    public String getDocument_number() {
        return document_number;
    }

    public void setDocument_number(String document_number) {
        this.document_number = document_number;
    }

    public String getDocument_type() {
        return document_type;
    }

    public void setDocument_type(String document_type) {
        this.document_type = document_type;
    }

    public String getDue_date() {
        return due_date;
    }

    public void setDue_date(String due_date) {
        this.due_date = due_date;
    }

    public String getOutstanding_amount() {
        return outstanding_amount;
    }

    public void setOutstanding_amount(String outstanding_amount) {
        this.outstanding_amount = outstanding_amount;
    }

    public String getPrincipal_code() {
        return principal_code;
    }

    public void setPrincipal_code(String principal_code) {
        this.principal_code = principal_code;
    }

    public String getTotal_amount() {
        return total_amount;
    }

    public void setTotal_amount(String total_amount) {
        this.total_amount = total_amount;
    }

    public String getTransaction_date() {
        return transaction_date;
    }

    public void setTransaction_date(String transaction_date) {
        this.transaction_date = transaction_date;
    }

    public String getCreated_by() {
        return created_by;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public String getCreated_date() {
        return created_date;
    }

    public void setCreated_date(String created_date) {
        this.created_date = created_date;
    }

    public String getModified_by() {
        return modified_by;
    }

    public void setModified_by(String modified_by) {
        this.modified_by = modified_by;
    }

    public String getModified_date() {
        return modified_date;
    }

    public void setModified_date(String modified_date) {
        this.modified_date = modified_date;
    }
}
