package com.autobahn.fsce.fsce.entity;

import javax.persistence.*;


@Entity
@Table(name = "invoice")
public class InvoiceEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String currency;

    private String dealercode;

    private String dealername;

    private String deliverydate;

    private String documentnumber;

    private String documenttype;

    private String duedate;

    private String outstandingamount;

    private String principalcode;

    private String totalamount;

    private String transactiondate;

    private String createdat;

    private String createdby;

    private String updatedat;

    private String updatedby;

    public InvoiceEntity() {
    }


    public InvoiceEntity( String currency, String dealercode, String dealername, String deliverydate, String documentnumber,
                          String documenttype, String duedate, String outstandingamount,
                          String principalcode, String totalamount, String transactiondate,
                          String createdat, String createdby, String updatedat, String updatedby) {
        
        this.currency = currency;
        this.dealercode = dealercode;
        this.dealername = dealername;
        this.deliverydate = deliverydate;
        this.documentnumber = documentnumber;
        this.documenttype = documenttype;
        this.duedate = duedate;
        this.outstandingamount = outstandingamount;
        this.principalcode = principalcode;
        this.totalamount = totalamount;
        this.transactiondate = transactiondate;
        this.createdat=createdat;
        this.createdby=createdby;
        this.updatedat= updatedat;
        this.updatedby = updatedby;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getDealercode() {
        return dealercode;
    }

    public void setDealercode(String dealercode) {
        this.dealercode = dealercode;
    }

    public String getDealername() {
        return dealername;
    }

    public void setDealername(String dealername) {
        this.dealername = dealername;
    }

    public String getDeliverydate() {
        return deliverydate;
    }

    public void setDeliverydate(String deliverydate) {
        this.deliverydate = deliverydate;
    }

    public String getDocumentnumber() {
        return documentnumber;
    }

    public void setDocumentnumber(String documentnumber) {
        this.documentnumber = documentnumber;
    }

    public String getDocumenttype() {
        return documenttype;
    }

    public void setDocumenttype(String documenttype) {
        this.documenttype = documenttype;
    }

    public String getDuedate() {
        return duedate;
    }

    public void setDuedate(String duedate) {
        this.duedate = duedate;
    }

    public String getOutstandingamount() {
        return outstandingamount;
    }

    public void setOutstandingamount(String outstandingamount) {
        this.outstandingamount = outstandingamount;
    }

    public String getPrincipalcode() {
        return principalcode;
    }

    public void setPrincipalcode(String principalcode) {
        this.principalcode = principalcode;
    }

    public String getTotalamount() {
        return totalamount;
    }

    public void setTotalamount(String totalamount) {
        this.totalamount = totalamount;
    }

    public String getTransactiondate() {
        return transactiondate;
    }

    public void setTransactiondate(String transactiondate) {
        this.transactiondate = transactiondate;
    }

    public String getCreatedat() {
        return createdat;
    }

    public void setCreatedat(String createdat) {
        this.createdat = createdat;
    }

    public String getCreatedby() {
        return createdby;
    }

    public void setCreatedby(String createdby) {
        this.createdby = createdby;
    }

    public String getUpdatedat() {
        return updatedat;
    }

    public void setUpdatedat(String updatedat) {
        this.updatedat = updatedat;
    }

    public String getUpdatedby() {
        return updatedby;
    }

    public void setUpdatedby(String updatedby) {
        this.updatedby = updatedby;
    }
}
