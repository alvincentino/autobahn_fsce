package com.autobahn.fsce.fsce.repository;


import com.autobahn.fsce.fsce.entity.DeductionEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DeductionRepository extends JpaRepository<DeductionEntity, Long> {

}
