package com.autobahn.fsce.fsce.repository;


import com.autobahn.fsce.fsce.entity.InvoiceEntity;
import org.springframework.data.jpa.repository.JpaRepository;


public interface InvoiceRepository extends JpaRepository<InvoiceEntity, String> {

}
