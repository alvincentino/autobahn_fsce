package com.autobahn.fsce.fsce;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FsceApplication {

	public static void main(String[] args) {
		SpringApplication.run(FsceApplication.class, args);
	}

}
