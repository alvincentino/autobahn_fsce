package com.autobahn.fsce.fsce.controller;

import com.autobahn.fsce.fsce.entity.DeductionEntity;
import com.autobahn.fsce.fsce.repository.DeductionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;


@RestController
public class DeductionController {


    @Autowired
     DeductionRepository deductionRepository;

    @PostMapping("v1/back-office/deductions")
    public DeductionEntity createDeduction(@RequestBody DeductionEntity deductionEntity){
        deductionRepository.save(deductionEntity);
        return deductionEntity;
    }

    @GetMapping("v1/{principal_id}/deductions")
    public List<DeductionEntity> getAllDeduction(@PathVariable Long principal_id){
        //return deductionRepository.findAll();
        return deductionRepository.findAll();
    }

    @DeleteMapping("v1/deductions/{id}")
    public void delete(@PathVariable Long id){
        deductionRepository.deleteById(id);
    }

    @PutMapping("v1/back-office/deductions/{id}")
    public void updateDeduction(@PathVariable Long id, DeductionEntity deductionEntity){
        deductionRepository.save(deductionEntity);
    }

}
