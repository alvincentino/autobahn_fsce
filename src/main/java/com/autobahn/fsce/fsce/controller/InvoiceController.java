package com.autobahn.fsce.fsce.controller;


import com.autobahn.fsce.fsce.entity.InvoiceEntity;
import com.autobahn.fsce.fsce.model.Invoice.FailedInvoice;
import com.autobahn.fsce.fsce.model.Invoice.InvoiceModel;
import com.autobahn.fsce.fsce.model.Invoice.InvoiceModelx;
import com.autobahn.fsce.fsce.model.Invoice.InvoiceSuccess;
import com.autobahn.fsce.fsce.repository.InvoiceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@RestController
public class InvoiceController {

    private List<InvoiceSuccess> invoiceSuccesses = new ArrayList<>();
    private List<FailedInvoice> failedInvoices = new ArrayList<>();
    private InvoiceModel invoiceModel = new InvoiceModel();

    ArrayList<String> documentNumber = new ArrayList<>();
    ArrayList<String> principalCode = new ArrayList<>();

    private static String UPLOAD_FOLDER = "C:/test/";

    @Autowired
    InvoiceRepository invoiceRepository;

    @PostMapping("/create")
    public InvoiceEntity createInvoice (@RequestBody InvoiceEntity invoice){

        String createdat = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
        String updatedat = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());

// save a single Customer
        invoiceRepository.save(new InvoiceEntity(invoice.getDocumentnumber(), invoice.getDocumenttype(),invoice.getTransactiondate(),invoice.getDeliverydate(),
                invoice.getDuedate(),invoice.getCurrency(),invoice.getOutstandingamount(),invoice.getTotalamount(),invoice.getDealername(),invoice.getDealercode(),
                invoice.getPrincipalcode(),createdat,null,updatedat,null));

        return invoice ;
    }

    @GetMapping("/api/v1/invoices")
    public List<InvoiceEntity> getInvoice(){
        return invoiceRepository.findAll();
    }

    @PostMapping("/api/v1/invoices")
    public InvoiceModel fileUpload(@RequestParam("file") MultipartFile file,@RequestParam("id") String id, RedirectAttributes redirectAttributes) {
        String returnString = "null";
        invoiceSuccesses.clear();
        failedInvoices.clear();


        String updatedby = id;
        String createdby = id;
        String createdat = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
        String updatedat = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());

        Date timeStampDate = new Date();
        long time = timeStampDate.getTime();

        DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
        Date date = new Date();
        String XcurrentDate = dateFormat.format(date);
        Date currentDate = null;
        try {
            currentDate = dateFormat.parse(XcurrentDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        System.out.println(currentDate);

      /*if (!file.getContentType().equals("text/csv")) {
            invoiceModel.setStatus("File is not csv");
        }
        if (file.isEmpty()) {
            invoiceModel.setStatus("No file selected");}*/
        String content = "";
        try {
            invoiceModel.setStatus("Failed to parse csv.");
            // read and write the file to the selected location-
            byte[] bytes = file.getBytes();
            //Path path = Paths.get(UPLOAD_FOLDER + file.getOriginalFilename()); //saving file localy(in your machine)
            //Files.write(path, bytes);//saving file localy(in your machine)
            content = new String(bytes);

            content = content.replace("\r", "");
            content = content.replace("\f", "");
            content = content.replace("\t", "");
            String[] rows = content.split("\n");



            for (int i = 0; i < rows.length; i++) {
                if (i == 0) {
                    continue; //skip first row
                }
                String[] col = rows[i].split(",");
                InvoiceSuccess invoiceSuccess = new InvoiceSuccess(col[0], col[1], col[2], col[3], col[4], col[5], col[6], col[7], col[8], col[9], col[10]);
                FailedInvoice failedInvoice = new FailedInvoice(col[0], col[1], col[2], col[3], col[4], col[5], col[6], col[7], col[8], col[9], col[10]);

                String document_number = invoiceSuccess.getDocumentNumber();//<<<<<<<<<<<<<<<<<<<<<<<<<<<<
                String document_type = invoiceSuccess.getDocumentType(); //done
                String transaction_date = invoiceSuccess.getTransactionDate();//done
                String delivery_date = invoiceSuccess.getDeliveryDate();//done
                String due_date = invoiceSuccess.getDueDate();//done
                String curency = invoiceSuccess.getCurrency(); //done
                String outstanding_amount = invoiceSuccess.getOutstandingAmount();//done check if numbers only
                String total_amount = invoiceSuccess.getTotalAmount();//done check if numbers only
                String dealer_name = invoiceSuccess.getDealerName(); //done
                String dealer_code = invoiceSuccess.getDealerCode(); //done
                String principal_code = invoiceSuccess.getPrincipalCode();//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

                documentNumber.add(document_number);
                Boolean boolDocumentNumber = true;
                for (int x = 0; x < documentNumber.size(); x++) {
                    if (document_number.equals(documentNumber.get(x))) {
                        boolDocumentNumber = false;
                    }
                }

                principalCode.add(principal_code);
                Boolean boolPrincipalCode = true;
                for (int y = 0; y < principalCode.size(); y++) {
                    if (principal_code.equals(principalCode.get(y))) {
                        boolPrincipalCode = false;
                    }
                }
                //Get current date and format




                //check first if nothing is empty
                if (document_number.isEmpty() || document_type.isEmpty() || transaction_date.isEmpty()
                        || delivery_date.isEmpty() || due_date.isEmpty() || outstanding_amount.isEmpty() || total_amount.isEmpty()
                        || dealer_name.isEmpty() || dealer_code.isEmpty() || principal_code.isEmpty()) {
                    System.out.println("One or more field is empty");
                    invoiceModel.setStatus("Ok");
                    failedInvoices.add(failedInvoice);
                } else {

                    //Validate dates
                    Boolean validDate = true;
                    Date Dtransaction_date = null;
                    Date Ddelivery_date = null;
                    Date Ddue_date = null;
                    try {
                        Dtransaction_date = dateFormat.parse(transaction_date);
                        Ddelivery_date = dateFormat.parse(delivery_date);
                        Ddue_date = dateFormat.parse(due_date);
                    } catch (ParseException e) {
                        validDate = false;
                    }

                    Boolean numberFormat = true;
                    try {
                        Double.parseDouble(outstanding_amount);
                        Double.parseDouble(total_amount);
                    } catch (NumberFormatException e) {
                        numberFormat = false;
                    }

                    //Check if valid document type
                    Boolean documentType = false;
                    if (document_type.equals("IN") || document_type.equals("DM") || document_type.equals("CM")) {
//                        System.out.println(Arrays.toString(col));
                        documentType = true;
                    }

                    try {
                        //complete validation
                        if (document_number.length() > 20 ||dealer_name.length() > 20 || dealer_code.length() > 20 || curency.length() > 3 ||
                                !validDate || !documentType || boolDocumentNumber || boolPrincipalCode ||
                                Dtransaction_date.after(currentDate ) || Ddelivery_date.after(currentDate) || Ddue_date.after(currentDate) ||
                                !numberFormat) {
                            System.out.println("Error:Fail");
                            invoiceModel.setStatus("Ok");
                            failedInvoices.add(failedInvoice);
                        } else {
                            invoiceRepository.save(new InvoiceEntity(col[0], col[1], col[2], col[3], col[4], col[5], col[6], col[7], col[8], col[9], col[10],createdat,createdby,updatedat,updatedby));
                            invoiceModel.setStatus("Ok");
                            invoiceSuccesses.add(invoiceSuccess);
                            System.out.println(Arrays.toString(col));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        invoiceModel.setSuccesses(invoiceSuccesses);
        invoiceModel.setFailed(failedInvoices);
        return invoiceModel;
    }


}
